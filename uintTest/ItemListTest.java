

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;


class ItemListTest {
	public  ItemList mylist=null; 
	public  ItemList mylist2 = null;

//	@Test
//	void testItemList() {
//	   
//	}
//
	@BeforeEach
	public  void init()  {
		System.out.println("Before Class");
		mylist = new ItemList();
		mylist2= new ItemList(4);
		
		 mylist.AddItem(5);
		 mylist.AddItem(4);
		 
		 mylist2.AddItem(2);
		 mylist2.AddItem(-1);
	}
	
	@Test
	public void testSize() {
		assertEquals("Checking size of List", 32, mylist.size());
		assertEquals("Checking size of List2", 4, mylist2.size());
	}


//
	@Test
	public void testAddItem() {
		mylist.AddItem(12);
		mylist2.AddItem(13);
		assertEquals("Checking AddItem of List", 3, mylist.capacity());
		assertEquals("Checking AddItem of List", 12, mylist.getItem(mylist.regularCount-1));
		
		assertEquals("Checking AddItem of List2", 3, mylist2.capacity());
		assertEquals("Checking AddItem of List2", 13, mylist2.getItem(mylist2.regularCount-1));
	}
//
	@Test
	public void testAddinPlaceItem() {
		mylist.AddinPlaceItem(10, 10);
		mylist2.AddinPlaceItem(11, 5);
		assertEquals("Checking AddinPlaceItem of List", 3, mylist.capacity());
		assertEquals("Checking AddinPlaceItem of List", 10, mylist.getItem(10));
		
		
		assertEquals("Checking AddinPlaceItem of List2", 3, mylist2.capacity());
		assertEquals("Checking AddinPlaceItem of List2", 11, mylist2.getItem(5));
	}
//
	@Test
	public void testGetItem() {
		
		assertEquals("Checking GetItem of List", 4, mylist.getItem(1));
		assertEquals("Checking GetItem of List2", 2, mylist2.getItem(0));
	}

	@Test
	public void testCapacity() {
		assertEquals("Checking Capacity of List", 2, mylist.capacity());
		assertEquals("Checking Capacity of List2", 2, mylist2.capacity());
		
	}
//	@Test
//	void testPrintArray() {
//		fail("Not yet implemented");
//	}


	
}
