
public class ItemList {
	int [] items;
	int count;
	int regularCount;

// Constructors
	public ItemList() {
		System.out.print("Hello to ArrayList Class");
		this.items = new int[32]; // Create an ArrayList object
		this.count = 0;
		System.out.println("ArrayList length " + this.items.length);

	}

	public ItemList(int size) {
		System.out.println("Hello to ArrayList Class");
		this.items = new int[size]; // Create an ArrayList object
		this.count = 0;
		System.out.println("ArrayList length " + this.items.length);

	}
//////////////////////////////////////////////////////////////////////////////

//Functions
	
	private void CheckSize(int count) {
		System.out.println("check size");
		if (this.items.length == count) {
			System.out.println("new size");
			int[] tmpArray = new int[this.items.length * 2];
			System.arraycopy(this.items, 0, tmpArray, 0, this.items.length);
			this.items = tmpArray;
			printArray(this.items);
			System.out.println("ArrayList Size after Resizing " + this.items.length);
		}
	    
	}

	void AddItem(int element) {
		this.CheckSize(this.count);
		this.items[this.count] = element;
		System.out.println("After adding " + this.items[this.count]);
		printArray(this.items);
		this.count++;
		this.regularCount++;
		

	}

	void AddinPlaceItem(int element, int index) {
		if (index >= this.items.length) {
			System.out.println("new size with big count");
			int[] tmpArray = new int[index + 1];
			System.arraycopy(this.items, 0, tmpArray, 0, this.items.length);
			this.items = tmpArray;
			this.count++;
			printArray(this.items);
			System.out.println("ArrayList Size after Resizing with new count " + this.items.length);

		}
		else if(index>this.regularCount) {
			this.count++;
			}
			else if (index==this.regularCount){
				this.regularCount++;
				this.count++;
			}
			
		this.items[index] = element;
		System.out.println("After adding " + this.items[index]);
		printArray(this.items);
		System.out.println("count" + this.count);
		System.out.println("regularCount" + this.regularCount);

	}

	int getItem(int index) {
		return this.items[index];

	}

	int capacity() {
		System.out.println(this.count);
		return this.count;
	}

	int size() {
		return this.items.length;
	}

	public static void printArray(int[] a) {
		System.out.print("[ ");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println("]");
	}
////////////////////////////////////////////////////////////////////////////////////	

}