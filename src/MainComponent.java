public class MainComponent {
	public static void main(String args[]) {
//with predefined size 
		 ItemList mylist = new ItemList(4);
		 mylist.AddItem(5);
		 mylist.AddItem(4);
		 mylist.AddItem(3);
		 mylist.AddItem(2);
		 mylist.AddItem(-1);
		 mylist.AddinPlaceItem(10, 10);
		 mylist.AddinPlaceItem(11, 5);
		 mylist.AddinPlaceItem(12, 11);
		 mylist.AddinPlaceItem(13, 8);
		 mylist.AddinPlaceItem(14, 20);
		 System.out.println("Capacity :"+ mylist.capacity());
		 System.out.println("Size :"+ mylist.size());
////////////////////////////////////////////////////////////////////////////////////
		 
//with no predefined size 
		 ItemList mylist2 = new ItemList();
		 mylist2.AddItem(5);
		 mylist2.AddItem(4);
		 mylist2.AddItem(3);
		 mylist2.AddItem(2);
		 mylist2.AddItem(-1);
		 mylist2.AddinPlaceItem(10, 10);
		 mylist2.AddinPlaceItem(11, 5);
		 mylist2.AddinPlaceItem(12, 11);
		 mylist2.AddinPlaceItem(13, 8);
		 mylist2.AddinPlaceItem(14, 20);
		 System.out.println("Capacity :"+ mylist2.capacity());
		 System.out.println("GetItem :"+ mylist2.getItem(5));
		 System.out.println("Size :"+ mylist2.size());
		 

		 
	 }
	

}
